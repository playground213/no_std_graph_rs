#![no_std]

const fn calc_edge_array_size(nodes: usize, edges_per_node: usize) -> usize {
    nodes * edges_per_node
}

//const fn add_node<const N: usize>(nodes: &mut [Node], index: usize) {}

pub struct GraphBuilder<const E: usize> {
    next_id: usize,
    nodes: [Option<Node>; E],
}

impl<const E: usize> GraphBuilder<E> {
    pub const fn new() -> GraphBuilder<E> {
        GraphBuilder {
            next_id: 0,
            nodes: [None; E],
        }
    }
    pub const fn create_node(self) -> (Self, NodeRef) {
        let id = self.next_id;
        let mut nodes = self.nodes;
        let node = Node {
            index: id,
            edge_low_index: 0,  // TODO
            edge_high_index: 0, // TODO
        };
        nodes[id] = Some(node);
        let node_ref = NodeRef { id };
        let builder = GraphBuilder {
            next_id: id + 1,
            nodes,
        };

        (builder, node_ref)
    }
}

pub struct Graph<const N: usize, const E: usize> {
    nodes: [Option<Node>; N],
    edges: [Option<Edge>; E],
    node_index: usize,
}

impl<const N: usize, const E: usize> Graph<N, E> {
    fn create() -> Self {
        Graph {
            nodes: [None; N],
            edges: [None; E],
            node_index: 0,
        }
    }
    fn add_node(&mut self) -> NodeRef {
        let index = self.node_index;
        let node = Node {
            index: index,
            edge_high_index: index * E,
            edge_low_index: index * E,
        };
        self.nodes[index] = Some(node);
        self.node_index += 1;
        NodeRef { id: index }
    }
    fn add_edge(&mut self, from: NodeRef, to: NodeRef) -> Option<EdgeRef> {
        let node = &mut self.nodes[from.id]?;
        let edge_index = node.edge_high_index;
        let edge = Edge {
            index: edge_index,
            from: from.id,
            to: to.id,
        };
        self.edges[edge_index] = Some(edge);
        node.edge_high_index = edge_index + 1;
        Some(EdgeRef { id: edge_index })
    }
    // fn create_node(&'a mut self) -> &'b Node<'a, N, E> {
    //     let mut nodes_ref = &mut self.nodes;
    //     let graph_ref: &'a Graph<'a, N, E> = self;
    //     let node = Node::<'a, N, E> {
    //         index: self.node_index,
    //         graph_ref: graph_ref,
    //     };
    //     let index = self.node_index;
    //     nodes_ref[index] = Some(node);
    //     let node_ref = &self.nodes[index].unwrap();
    //     self.node_index += 1;
    //     node_ref
    // }
}

pub struct ImmutableGraph {}

pub struct NodeRef {
    id: usize,
}

pub struct EdgeRef {
    id: usize,
}

//pub struct NodeRef<'a, const N: usize, const E: usize> {
//    id: u32,
//    graph_ref: &'a Graph<N, E>,
//}

#[derive(Copy, Clone)]
struct Node {
    index: usize,
    edge_low_index: usize,
    edge_high_index: usize,
}

#[derive(Copy, Clone)]
pub struct Edge {
    index: usize,
    from: usize,
    to: usize,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
